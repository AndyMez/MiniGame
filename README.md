# Mini Game

## _3eme projet :_

Pour ce troisième projet, j'ai dû réaliser un jeu de mon choix en JavaScript.
J'ai tout d'abord commencé à réfléchir à quel jeu pourrait me plaire, tout en étant capable d'y insérer les fonctionnalités de bases.
Les règles de ce projet étaient plutôt simples, nous avions carte blanche, mais nous devions au moins nous servir d'une ou deux classes et avoir des éléments qui seront modifiés au fil du jeu. (Tels que des barres de points de vie, progression, score ...)

Pour ce projet, j'ai rencontré de nombreux problèmes et beaucoup d'aspects du jeu ne fonctionne pas comme je le souhaiterais dû à du manque de temps et de compétences, souhaitant me rapprocher au plus du jeu officiel :

- Je n'ai malheureusement pas réussi à implanter les multiplicateurs élémentaires, formules de dégâts (selon l'élément, défense ...), prise en compte de l'initiative.
- Les barres de points de vies sont en base 100 pour les deux joueurs. (dû à un problème d'affichage lorsque quelqu'un perdait des points de vies)
- L'opposant ne possède pas de réels tours, mais de "ripostes" lorsqu'il subit une attaque du joueur
- Dégâts des joueurs fixes. (formule de dégâts officielle non implanté, du coup la valeur des compétences divisé par un certain montant spécifique). De plus, l'opposant répond aux attaques par des attaques précises. (compétence utilisé non-random et pas de réel logique derrière celles ci)
- Utilisations des fonctions trop tard. Mon code aurait pu être optimisé sur plusieurs points.
- Utilisations des classes quand j'avais bien trop avancé dans mon projet. J'ai ne trouvais plus de réels utilisations à les utilisés ...

## Caractéristiques

- Fonctionnel, mais manque d'avantages de fonctionnalités ...

### Maquettes

[Mes Maquettes](/public/design) \
[Lien du projet](https://andymez.gitlab.io/MiniGame/)