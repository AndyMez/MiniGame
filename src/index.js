
// import { Array } from "./game/Array";
// import { Animations } from "./game/Animations";
// import { Damage } from "./game/Damage";
// import { Skills } from "./game/Skills";


// Tableau du Pokemon opposant 
let opponent = [
  {
    name : 'Balstoise',
    type : 'Water',
    level : 50,
    HP : 100,
    Attack : 134,
    Defense : 151,
    Special : 136,
    Speed : 129,
    skills : [
      {
        skillName: 'Surf',
        skillPotency : 90,
        skillAccuracy : 100,
        skillElement : 'Water',
      },
      {
        skillName : 'Blizzard',
        skillPotency : 110,
        skillAccuracy : 70,
        skillElement : 'Ice',
      },
      {
        skillName : 'Earthquake',
        skillPotency : 100,
        skillAccuracy : 100,
        skillElement : 'Ground',
      },
      {
        skillName : 'Rest',
        skillPotency : 80,
        skillAccuracy : 0,
        skillElement : 'Psychic',
      }
    ]
  }
]

// Tableau du Pokemon du joueur
let player = [
  {
    name : 'Charizard',
    type : 'Fire',
    level : 50,
    HP : 100,
    Attack : 135,
    Defense : 129,
    Special : 136,
    Speed : 151,
    skills : [
      {
        skillName: 'Fire Blast',
        skillPotency : 110,
        skillAccuracy : 85,
        skillElement : 'Fire',
      },
      {
        skillName: 'Earthquake',
        skillPotency: 100,
        skillAccuracy: 100,
        skillElement: 'Ground',
      },
      {
        skillName : 'Swords Dance',
        skillPotency: 90,
        skillAccuracy: 90,
        skillElement : 'Normal',
      },
      {
        skillName : 'Body Slam',
        skillPotency : 85,
        skillAccuracy : 100,
        skillElement : 'Normal',
      }
    ]
  }
]


let startBtn = document.querySelector('#start');
let game = document.querySelector('.game');
let accueil = document.querySelector('.accueil-panel');
let fight = document.querySelector('#fight');
let skillsPanel = document.querySelector('.skills');
let possibilities = document.querySelector('.possibilities');
let skillsDetails = document.querySelector('.skills-details');
let returnBtn = document.querySelector('#return');
let firstPlayerSkill = document.querySelector('#firstPlayerSkill');
let secondPlayerSkill = document.querySelector('#secondPlayerSkill');
let thirdPlayerSkill = document.querySelector('#thirdPlayerSkill');
let fourthPlayerSkill = document.querySelector('#fourthPlayerSkill');
let element = document.querySelector('.element');
let opponentBar = document.querySelector('.opponent-bar');
let playerBar = document.querySelector('.player-bar');
let opponentPokemon = document.querySelector('.opponent-portrait');
let playerPokemon = document.querySelector('.player-portrait');
let playerCard = document.querySelector('.player-card');
let opponentCard = document.querySelector('.opponent-card');
let endPanel = document.querySelector('.end-panel');
let victory = document.querySelector('.victory');
let defeat = document.querySelector('.defeat');




// Retrait de l'accueil et affichage du jeu 
startBtn.style.display =  'inline';

startBtn.addEventListener('click', () => {
  if (startBtn.style.display === 'inline'){
    startBtn.style.display = 'none'; 
    accueil.style.display = 'none'; 
    game.style.display = 'block';
  } 
})


// Affichage des compétences
fight.style.display = 'flex';

fight.addEventListener('click', () => {
  if (fight.style.display === 'flex') {
    skillsPanel.style.visibility = 'visible'; 
    skillsDetails.style.display = 'grid';
    possibilities.style.display = 'none'; 
  }
})


// Retour aux choix des possibilitiés 
fight.style.display = 'flex';

returnBtn.addEventListener('click', () => {
  if (fight.style.display === 'flex') {
    skillsPanel.style.visibility = 'hidden'; 
    skillsDetails.style.display = 'none';
    possibilities.style.display = 'grid'; 
  }
})



// Message de fin (Victoire / Défaite)
function endPanelWin() {
  endPanel.style.display = 'block'
  victory.style.display = 'inline'
}

function endPanelLoose() {
  endPanel.style.display = 'block'
  defeat.style.display = 'inline'
}



// Affichage des sorts 
firstPlayerSkill.textContent = player[0].skills[0].skillName
secondPlayerSkill.textContent = player[0].skills[1].skillName
thirdPlayerSkill.textContent = player[0].skills[2].skillName
fourthPlayerSkill.textContent = player[0].skills[3].skillName


// Affichage des éléments et de leurs background
firstPlayerSkill.addEventListener('mouseenter', () => {
  element.style.background = "#ff5635"
  element.textContent = "Fire"
})
secondPlayerSkill.addEventListener('mouseenter', () => {
  element.style.background = "#dcba54"
  element.textContent = "Ground"
})
thirdPlayerSkill.addEventListener('mouseenter', () => {
  element.style.background = "#aaaa99"
  element.textContent = "Normal"
})
fourthPlayerSkill.addEventListener('mouseenter', () => {
  element.style.background = "#aaaa99"
  element.textContent = "Normal"
})

// Réalisation des dégâts & Dégât de l'opposant (Counter Attack)
// Retrait des points & Réduit la barre d'HP
function playerDealDamage() {
  opponentBar.style.width = opponent[0].HP + 'px';
  if (opponent[0].HP <= 0) {
    opponentBar.style.width = 0
  }
}
function opponentDealDamage() {
  playerBar.style.width = player[0].HP + 'px';
  if (player[0].HP <= 0) {
    playerBar.style.width = 0
  }
}

// Empêche d'infligé des dégâts supplémentaire quand les HP sont à 0 (meh ...)
function noDamage() {
  player[0].skills[0].skillPotency = 0
  opponent[0].skills[0].skillPotency = 0
  player[0].skills[1].skillPotency = 0
  opponent[0].skills[1].skillPotency = 0
  player[0].skills[2].skillPotency = 0
  opponent[0].skills[2].skillPotency = 0
  player[0].skills[3].skillPotency = 0
  opponent[0].skills[3].skillPotency = 0
}

// Retire le Pokemon battu ainsi que la plaque d'information
function opponentFainted() {
  if (opponent[0].HP <= 0) {
    opponentCard.style.left = "-100%"
    opponentPokemon.style.right = "-100%"
    setTimeout(() => {
      endPanelWin()
    }, 1000);
  }
} 

function playerFainted() {
  if (player[0].HP <= 0) {
    console.log(player[0].HP);
    playerCard.style.right = "-100%"
    playerPokemon.style.left = "-100%"
    setTimeout(() => {
      endPanelLoose()
    }, 1000);
  }
} 


// Animation des attaques
function playerSlide() {
  playerPokemon.classList.add('slide-animation')
  setTimeout(() => {
    playerPokemon.classList.remove('slide-animation')
  }, 500);
}

function opponentSlide() {
  opponentPokemon.classList.add('slide-anim-opponent')
  setTimeout(() => {
    opponentPokemon.classList.remove('slide-anim-opponent')
  }, 500);
}

// Animation des attaques subis
function damageTaken() {
  setTimeout(() => {
    opponentPokemon.classList.add('damage-taken')
    setTimeout(() => {
      opponentPokemon.classList.remove('damage-taken')
    }, 500);
  }, 1000);
}


// Event : Utilisations des compétences
firstPlayerSkill.addEventListener('click', () => {
  opponent[0].HP -= player[0].skills[0].skillPotency /5
  player[0].HP -= opponent[0].skills[3].skillPotency /6
  playerSlide()
  damageTaken()

  if (opponent[0].HP >= 0 || player[0].HP >= 0) {
    playerDealDamage()
    setTimeout(() => {
      opponentSlide()
      setTimeout(() => {
        opponentDealDamage()
      }, 1000);
    }, 2000);
  } 

  if (opponent[0].HP <= 0 || player[0].HP <= 0) {
    noDamage()
    opponentFainted() 
    playerFainted()
  }
});


secondPlayerSkill.addEventListener('click', () => {
  opponent[0].HP -= player[0].skills[1].skillPotency /5
  player[0].HP -= opponent[0].skills[2].skillPotency /4
  playerSlide()
  damageTaken()

  if (opponent[0].HP >= 0 || player[0].HP >= 0) {
    playerDealDamage()
    setTimeout(() => {
      opponentSlide()
      setTimeout(() => {
        opponentDealDamage()
      }, 1000);
    }, 2000);
  } 

  if (opponent[0].HP <= 0 || player[0].HP <= 0) {
    noDamage()
    opponentFainted() 
    playerFainted()
  }
});

thirdPlayerSkill.addEventListener('click', () => {
  opponent[0].HP -= player[0].skills[2].skillPotency /4
  player[0].HP -= opponent[0].skills[0].skillPotency /5
  playerSlide()
  damageTaken()

  if (opponent[0].HP >= 0 || player[0].HP >= 0) {
    playerDealDamage()
    setTimeout(() => {
      opponentSlide()
      setTimeout(() => {
        opponentDealDamage()
      }, 1000);
    }, 2000);
  } 

  if (opponent[0].HP <= 0 || player[0].HP <= 0) {
    noDamage()
    opponentFainted() 
    playerFainted()
  }
});

fourthPlayerSkill.addEventListener('click', () => {
  opponent[0].HP -= player[0].skills[3].skillPotency /5
  player[0].HP -= opponent[0].skills[1].skillPotency /4
  playerSlide()
  damageTaken()

  if (opponent[0].HP >= 0 || player[0].HP >= 0) {
    playerDealDamage()
    setTimeout(() => {
      opponentSlide()
      setTimeout(() => {
        opponentDealDamage()
      }, 1000);
    }, 2000);
  } 

  if (opponent[0].HP <= 0 || player[0].HP <= 0) {
    noDamage()
    opponentFainted() 
    playerFainted()
  }
});

